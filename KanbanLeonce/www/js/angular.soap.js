angular.module('angularSoap', [])
.factory("$soap",['$q',function($q){
	return {
		post: function(url, action, params){
			var deferred = $q.defer();
			//Create SOAPClientParameters
			var soapParams = new SOAPClientParameters();
			for(var param in params){
				soapParams.add(param, params[param]);
			}
			function GetSoapResponse_callBack(r, soapResponse)
			{
			deferred.resolve(soapResponse);
			}
			SOAPClient.invoke(url, action, soapParams, true, GetSoapResponse_callBack);
			return deferred.promise;
		},
		setCredentials: function(username, password){
			SOAPClient.username = username;
			SOAPClient.password = password;
		}
	}
}]);
