angular.module('klapp.datafactory' , ['angularSoap'])
.factory('KLDataFactory', function ($soap ) {
	var base_url = "http://localhost:8080/ADInterface/services/ModelADService";
	var param = {"_0:queryData":
	   { "_0:ModelCRUDRequest":{
	       "_0:ModelCRUD":{
	           "_0:serviceType": "CreditStatus", 
	           "_0:TableName":"C_BPartner" 
	                   }, 
	       "_0:ADLoginRequest": {
	           "_0:user":"WebService",
	           "_0:pass":"WebService",
	           "_0:lang":"192",
	           "_0:ClientID":"11",
	           "_0:RoleID":"50004",
	           "_0:OrgID":"11",
	           "_0:WarehouseID":"103",
	           "_0:stage":"0"}
	}}
	           };
	 
	 return {
	        GetCreditStatus: function(){
	            return $soap.post(base_url,"",param 
	            		)
	        }, 
			GetUsers: function(dataparam){
            return $soap.post(base_url,"",dataparam 
            		);
        }
	    };
	})
	
	
	