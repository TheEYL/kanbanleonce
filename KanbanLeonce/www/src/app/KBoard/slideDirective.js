angular.module('starter')

.directive('jnLibDetectGestures', ['$ionicGesture',function ($ionicGesture) {
	            return {
	                restrict: 'A',

	                link: function (scope, elem, attrs) {
	                    var gestureType = attrs.gestureType;
	                  
	              
	                    switch (gestureType) {
	                        case 'swipe':
	                            $ionicGesture.on('swipe', scope.handleSwipeGesture, elem);
	                            break;
	                        case 'nextbordright':
	                            $ionicGesture.on('pan', scope.handleSwipeLeftGesture, elem);
	                            break;
	                        case 'nextbordleft':
	                            $ionicGesture.on('pan', scope.handleSwipeRightGesture, elem);
	                            break;
	                        case 'swiperight':
	                            $ionicGesture.on('swiperight', scope.handleSwipeRightGesture, elem);
	                            break;
	                        case 'swipeleft':
	                            $ionicGesture.on('swipeleft', scope.handleSwipeLeftGesture, elem);
	                            break;
	                        case 'doubletap':
	                            $ionicGesture.on('doubletap', scope.handleDoubleTapGesture, elem);
	                            break;
	                        case 'tap':
	                            $ionicGesture.on('tap', scope.handleTapGesture, elem);
	                            break;
	                        case 'scroll':
	                            $ionicGesture.on('scroll', scope.handleScrollGesture, elem);
	                            break;
	                    }

	                }
	            }
	        }]);
   