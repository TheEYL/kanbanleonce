
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var main_module= angular.module('starter', ['ionic', 'ngDraggable','klapp.datafactory'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  })
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  
  .state('kboard', {
    url: '/kboard',
    templateUrl: 'src/app/KBoard/KBoard.html',
    controller: 'MainCtrl'
  })
  .state('soap', {
      url: "/soap",
      abstract: true,
      templateUrl: "src/app/soaptest/tab/tabs.html",
      controller: 'TabsCtrl'	  
    })
    .state('soap.home', {
      url: "/home",
      views: {
        'GetBP-tab': {
          templateUrl: "src/app/soaptest/tab/home-tabs.html",
          controller: 'soapGetCtrl'
        }
      }
    })
    .state('soap.create', {
      url: "/create",
      views: {
        'CreateBP-tab': {
          templateUrl: "src/app/soaptest/tab/create-tabs.html",
          controller: 'soapCreateCtrl'
        }
      }
    }) 
    .state('soap.location', {
      url: "/location",
      views: {
        'Locations-tab': {
          templateUrl: "src/app/soaptest/tab/locations-tabs.html",
          controller: 'soapLocationCtrl'
        }
      }
    }) 
    .state('soap.update', {
      url: "/update",
      views: {
        'Update-tab': {
          templateUrl: "src/app/soaptest/tab/update-tabs.html",
          controller: 'soapUdateCtrl'
        }
      }
    }) 
    .state('soap.createupdate', {
      url: "/createupdate",
      views: {
        'createupdate-tab': {
          templateUrl: "src/app/soaptest/tab/createupdate-tabs.html",
          controller: 'soapCreateUpdateCtrl'
        }
      }
    })
    .state('soap.delete', {
      url: "/delete",
      views: {
        'Delete-tab': {
          templateUrl: "src/app/soaptest/tab/delete-tabs.html",
          controller: 'soapDeleteCtrl'
        }
      }
    }) ;
	$urlRouterProvider.otherwise('/kboard')
})   



